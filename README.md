This playbook can be run for two different reasons:
    
    - Reset the Kubernetes cluster and reintailize the cluster
      run: ansible-playbook playbook.yml -i inventory -v

    - Checkup if all firewall ports, required files, prerequisites are configured properly without resetting the Kubernetes cluster
      run: ansible-playbook playbook.yml -i inventory --extra-vars "reset=yes"
      
Variables to configure depending on your setup before running the playbook are all gathered in the folling files
    - inventory
    - group-vars/servers/vars
    - group-vars/masters/vars
    - group-vars/nodes/vars
    - roles/kubernetes/templates/config.yaml
